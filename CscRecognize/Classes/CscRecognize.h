//
//  CscRecognize.h
//  Pods
//
//  Created by Pongpop Inkaew on 6/8/2560 BE.
//
//

#import <Foundation/Foundation.h>

@interface CscRecognize : NSObject
+ (void) activate:(NSString*)licenseKey;
@end
