//
//  FaceEnrollProcessView.h
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import "FaceProcessView.h"
#include "LuxandFaceSDK.h"

#define MAX_ENROLL_COUNT_NO 3
#define BLINK_MIN_PERCENT = 20;
#define OPEN_EYES_PERCENT = 80;


@interface FaceEnrollViewModel : NSObject

@property(strong,atomic) NSData *captureImage1;
@property(strong,atomic) NSData *captureImage2;
@property(strong,atomic) NSData *captureImage3;

@property(strong,atomic) NSData *eyesTemplate;
@property(strong,atomic) NSData *eyesTemplate2;
@property(strong,atomic) NSData *eyesTemplate3;
@property(strong,atomic) NSData *facialTempalte;
@property(strong,atomic) NSData *facialTempalte2;
@property(strong,atomic) NSData *facialTempalte3;

@end

@implementation FaceEnrollViewModel

@end

@protocol FaceEnrollProcessViewDelegate
- (void) onFaceEnrollCaptureFaceImage:(UIImage*)image countNo:(int)countNo;
- (void) onFaceEnrollNotMatch;
- (void) onFaceEnrollSuccess:(FaceEnrollViewModel*)faceEnrollViewModel;
- (void) onFaceEnrollResetView;
@end

@protocol FaceEnrollBarManager
- (void) showTouchView:(int)countNo;
- (void) hideTouchView:(int)countNo;
- (void) resetView;
- (void) showCaptureImage:(UIImage*)image countNo:(int)countNo;
@end

@interface FaceEnrollProcessView : FaceProcessView <FaceProcessViewDelegate>
{
    NSLock *arrayLock;
    FaceEnrollViewModel * enrollModel;
    NSArray * faceConstArrayTemp;
    int enrollCountNo;
    UIColor* whiteColor;
    UIColor* blueColor;
}
-(void) startEnroll;
-(void) pauseEnroll;
-(void) resetView;
@property(nonatomic, assign) id<FaceRecognitionCriteria> criteria;
@property(nonatomic, assign) id<FaceEnrollProcessViewDelegate> viewDelegate;
@property(atomic,assign) id<FaceEnrollBarManager> enrollBarView;
//@property(nonatomic, assign) id<EyeBlinkDelegate> eyeBlinkDelegate;
@property(assign,atomic) NSMutableArray * faceEnrollTempArray;
@end
