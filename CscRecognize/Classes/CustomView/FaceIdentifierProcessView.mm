//
//  FaceIdentifierProcessView.m
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import "FaceIdentifierProcessView.h"

@interface FoundTemplateTemp : NSObject

@property(strong,atomic) UIImage *captureImage;
@property(strong,atomic) NSMutableArray * foundArray;

@end

@implementation FoundTemplateTemp
-(void)dealloc{
    //NSLog(@"FoundTemplateTemp : dealloc");
    [super dealloc];
}
@end

@implementation FaceIdentifierProcessView

@synthesize viewDelegate;
@synthesize criteria;

NSDictionary *templateViewDic;
NSUInteger templateViewSize;
volatile int faceMatching;
FoundTemplateTemp * foundTemplateTemp;

-(void)initView:(UIView *)parentUIView{
    self.delegate = self;
    [super initView:parentUIView];
    [self pauseIdentifier];
    [self refresh];
    foundTemplateTemp = [[FoundTemplateTemp alloc]init];
    whiteColor = [UIColor whiteColor];
    blueColor = [UIColor blueColor];
    
}

-(void) startIdentifier{
    [super setPauseProcess:NO];
    //[super setClosing:NO];
}

-(void) pauseIdentifier{
    [super setPauseProcess:YES];
}

-(void) refresh{
    templateViewSize = 0;
    faceMatching = NO;
    templateViewSize = [criteria getTemplateViewModelDictionary].count;
}


-(void) dealloc{
    //NSLog(@"FaceIdentifierProcessView dealloc");
    [[foundTemplateTemp foundArray] removeAllObjects];
    [[foundTemplateTemp foundArray] release];
    [foundTemplateTemp release];
    foundTemplateTemp = nil;
    [super destroy];
    [super dealloc];
}

-(void)onFaceTouched:(HImage)hImage faceParam:(DetectFaceParams)faceParam{

}

-(void)onFaceNotDetected{
}
-(BOOL)allowFaceTouch{
    return FALSE;
}

-(void)onFaceDetected:(HImage)hImage FacialFeatures:(FSDK_Features *)FacialFeatures{
    // This method call in background thread
    if(templateViewSize <= 0 || faceMatching == YES || criteria == nil || [super pauseProcess] == YES)
        return;
    //Closure
    //BOOL (^ onEyeOpenedBlock )( HImage,FSDK_Features* );
    //onEyeOpenedBlock = ^{ };
    
    void (^ onBlinkEyeCompleted)(void) = ^{
        [self pauseIdentifier];
        //NSLog(@"onBlinkEyeCompleted");
        //[super setDetectSquareColor:UIColor.blueColor];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.viewDelegate onFound:[foundTemplateTemp foundArray] image:[foundTemplateTemp captureImage]];
        });;
    };
    
    BOOL (^ onEyeOpenedCompleted )( HImage,FSDK_Features* ) = ^(HImage image,FSDK_Features* features){
        //NSLog(@"onEyeOpenedCompleted");
        [self pauseIdentifier];
        [self showDetectLabel:@"" textColor:[UIColor greenColor]];
        BOOL isMatch = [self matchFace:image FacialFeatures:features];
        //[super setDetectSquareColor:whiteColor];
        [self startIdentifier];
        return isMatch;
    };
    
    [super checkBlinkEye:hImage features:FacialFeatures onEyeOpened:onEyeOpenedCompleted onBlinkEyeCompleted:onBlinkEyeCompleted];
    
}

-(void)onPauseProcessView{
    NSLog(@"onPauseProcessView");
}

-(BOOL) matchFace2:(HImage)hImage FacialFeatures:(FSDK_Features *)FacialFeatures{
    faceMatching = YES;
    UIImage *image = [super getUIImageFromBuffer:hImage faceParam:DetectFaceParams()];
    [foundTemplateTemp setCaptureImage:image];
    [foundTemplateTemp setFoundArray:nil];
    faceMatching = NO;
    return true;
}

-(BOOL) matchFace:(HImage)hImage FacialFeatures:(FSDK_Features *)FacialFeatures{
    FSDK_FaceTemplate compareTemplate;
    if(faceMatching == NO && FSDK_GetFaceTemplateUsingEyes(hImage, FacialFeatures, &compareTemplate) == FSDKE_OK ){
        faceMatching = YES;
        templateViewDic = [criteria getTemplateViewModelDictionary];
        NSArray * keys = [templateViewDic allKeys];
        NSMutableArray * foundArray = [[[NSMutableArray alloc] init]autorelease];
        BOOL found = false;
        for (NSString* key in keys) {
            FSDK_FaceTemplate faceTemplate;
            id<TemplateViewModel> tmplateModel = [templateViewDic objectForKey:key];
            for(NSData* data in [tmplateModel getEyeTemplates]){
                if(data==nil || data.length <= 0)
                    continue;
                
                [data getBytes:&faceTemplate length:sizeof(FSDK_FaceTemplate)];
                float similarity = 0;
                FSDK_MatchFaces(&compareTemplate, &faceTemplate, &similarity);
                if(similarity > [criteria similarity]){
#if defined(DEBUG)
                    NSLog(@"Similarity of '%@' = %f",key,similarity);
#endif
                    [foundArray addObject:tmplateModel];
                    if(![criteria findAnotherSameFace]){
                        found = true;
                        break;
                    }
                    
                }
            }
            if (found == TRUE) break;
        }
        if([foundArray count] > 0){
            [self pauseIdentifier];
            UIImage *image = [super getUIImageFromBuffer:hImage faceParam:DetectFaceParams()];
            /*dispatch_async(dispatch_get_main_queue(), ^{
                [self.viewDelegate onFound:foundArray image:image];
            });*/
            [foundTemplateTemp setCaptureImage:image];
            [foundTemplateTemp setFoundArray:foundArray];
        }
        faceMatching = NO;
        return [foundArray count] > 0;
    }

    return FALSE;
}

@end
