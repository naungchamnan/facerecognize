//
//  FaceIdentifierProcessView.h
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import "FaceProcessView.h"
#include "LuxandFaceSDK.h"

@protocol FaceIdentifierProcessViewDelegate
- (void) onFound:(NSMutableArray*)identifyFaceArray image:(UIImage*)image;
@end


@interface FaceIdentifierProcessView : FaceProcessView <FaceProcessViewDelegate>
{
    UIColor* whiteColor;
    UIColor* blueColor;
}
-(void) startIdentifier;
-(void) pauseIdentifier;
-(void) refresh;
@property(nonatomic, assign) id<FaceRecognitionCriteria> criteria;
@property(nonatomic, assign) id<FaceIdentifierProcessViewDelegate> viewDelegate;
@end

