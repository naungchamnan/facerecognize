//
//  FaceProcessView.m
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import "FaceProcessView.h"
#import "PodAsset.h"

enum {
    ATTRIB_VERTEX,
    ATTRIB_TEXTUREPOSITON,
    NUM_ATTRIBUTES
};

@implementation FaceProcessView

inline bool PointInRectangle(int point_x, int point_y, int rect_x1, int rect_y1, int rect_x2, int rect_y2)
{
    return (point_x >= rect_x1) && (point_x <= rect_x2) && (point_y >= rect_y1) && (point_y <= rect_y2);
}


-(int)getFaceFrame:(const FSDK_Features *)Features x1:(int *)x1 y1:(int *)y1 x2:(int *)x2 y2:(int *)y2{
    if (!Features || !x1 || !y1 || !x2 || !y2)
        return FSDKE_INVALID_ARGUMENT;
    
    float u1 = (float)(*Features)[0].x;
    float v1 = (float)(*Features)[0].y;
    float u2 = (float)(*Features)[1].x;
    float v2 = (float)(*Features)[1].y;
    float xc = (u1 + u2) / 2;
    float yc = (v1 + v2) / 2;
    int w = (int)pow((u2 - u1) * (u2 - u1) + (v2 - v1) * (v2 - v1), 0.5f);
    
    *x1 = (int)(xc - w * 1.6 * 0.9);
    *y1 = (int)(yc - w * 1.1 * 0.9);
    *x2 = (int)(xc + w * 1.6 * 0.9);
    *y2 = (int)(yc + w * 2.1 * 0.9);
    if (*x2 - *x1 > *y2 - *y1) {
        *x2 = *x1 + *y2 - *y1;
    } else {
        *y2 = *y1 + *x2 - *x1;
    }
    return 0;
}


- (void)resetTrackerParameters
{
    int errpos = 0;
    FSDK_SetFaceDetectionParameters(false, false, 100);
    FSDK_SetFaceDetectionThreshold(3);
    FSDK_SetTrackerMultipleParameters(_tracker, "ContinuousVideoFeed=true;FacialFeatureJitterSuppression=0;RecognitionPrecision=1;Threshold=0.996;Threshold2=0.9995;ThresholdFeed=0.97;MemoryLimit=2000;HandleArbitraryRotations=false;DetermineFaceRotationAngle=false;InternalResizeWidth=70;FaceDetectionThreshold=5;", &errpos);
#if defined(DEBUG)
    if (errpos)
        NSLog(@"FSDK_SetTrackerMultipleParameters returned errpos = %d", errpos);
#endif
}

@synthesize glView = _glView;
@synthesize tracker = _tracker;
@synthesize templatePath = _templatePath;
@synthesize closing = _closing;
@synthesize processingImage = _processingImage;
@synthesize pauseProcess = _pauseProcess;
@synthesize delegate;
@synthesize eyeBlinkDelegate;
static UIInterfaceOrientation old_orientation;

-(void)_init{
    old_orientation = UIInterfaceOrientationUnknown;
    faceDataLock = [[NSLock alloc] init];
    nameDataLock = [[NSLock alloc] init];
    enteredNameLock = [[NSLock alloc] init];
    idOfTouchedFaceLock = [[NSLock alloc] init];
    faceTouchLock = [[NSLock alloc] init];
    for (int i=0; i<MAX_FACES; ++i) names[i] = new char[MAX_NAME_LEN + 1];
    
    NSString * templatePathO = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Memory50.dat"];
    const char * templatePath = [templatePathO UTF8String];
    _templatePath = (char *)malloc(strlen(templatePath)+1);
    strcpy(_templatePath, templatePath);
#if defined(DEBUG)
    NSLog(@"using templatePath: %s", _templatePath);
#endif
    if (FSDKE_OK != FSDK_LoadTrackerMemoryFromFile(&_tracker, _templatePath))
        FSDK_CreateTracker(&_tracker);
    
    [self resetTrackerParameters];
    _processingImage = NO;
    _pauseProcess = NO;
    rotating = NO;
    videoStarted = 0;
    faceTouched = NO;
    idOfTouchedFace = -1;
    enteredName = NULL;
    clearTracker = NO;
    isFaceTouch = false;
    
    memset(faces, 0, sizeof(FaceRectangle)*MAX_FACES);
}

-(void)initView:(UIView *)parentUIView{
    [self _init];
    uiView = parentUIView;
    //uiView = self;
    
    _glView = [[RecognitionGLView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, 10.0f)];
    [uiView addSubview:_glView];
    [_glView release];
    
    [self loadVertexShader:@"DirectDisplayShader" fragmentShader:@"DirectDisplayShader" forProgram:&directDisplayProgram];
    // Creating MAX_FACES number of face tracking rectangles
    for (int i=0; i<MAX_FACES; ++i) {
        trackingRects[i] = [[CALayer alloc] init];
        trackingRects[i].bounds = CGRectMake(0.0f, 0.0f, 0.0f, 0.0f);
        trackingRects[i].cornerRadius = 0.0f;
        trackingRects[i].borderColor = [[UIColor blueColor] CGColor];
        trackingRects[i].borderWidth = 2.0f;
        trackingRects[i].position = CGPointMake(100.0f, 100.0f);
        trackingRects[i].opacity = 0.0f;
        trackingRects[i].anchorPoint = CGPointMake(0.0f, 0.0f); //for position to be the top-left corner
        nameLabels[i] = [[CATextLayer alloc] init];
        [nameLabels[i] setFont:@"Helvetica-Bold"];
        [nameLabels[i] setFontSize:20];
        [nameLabels[i] setFrame:CGRectMake(10.0f, 10.0f, 200.0f, 40.0f)];
        [nameLabels[i] setString:@""];
        [nameLabels[i] setAlignmentMode:kCAAlignmentLeft];
        [nameLabels[i] setForegroundColor:[[UIColor greenColor] CGColor]];
        [nameLabels[i] setAlignmentMode:kCAAlignmentCenter];
        [trackingRects[i] addSublayer:nameLabels[i]];
        [nameLabels[i] release];
    }
    
    // Disable animations for move and resize (otherwise trackingRect will jump)
    for (int i=0; i<MAX_FACES; ++i) {
        NSMutableDictionary * newActions = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"position", [NSNull null], @"bounds", nil];
        trackingRects[i].actions = newActions;
        [newActions release];
    }
    for (int i=0; i<MAX_FACES; ++i) {
        NSMutableDictionary * newActions = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"position", [NSNull null], @"bounds", nil];
        nameLabels[i].actions = newActions;
        [newActions release];
    }
    
    for (int i=0; i<MAX_FACES; ++i) {
        [_glView.layer addSublayer:trackingRects[i]];
    }
    
    camera = [[RecognitionCamera alloc] init];
    //camera.delegate = nil;
    camera.delegate = self; //we want to receive processNewCameraFrame messages
    [self cameraHasConnected]; //the method doesn't perform any work now
}

-(void)stopCamera{
    _closing = YES;
    //[camera release];
}

-(void)startCamera{
    _closing = NO;
    [self relocateSubviewsForOrientation:old_orientation];
}

-(void) simulateFaceTouch{
    [faceDataLock lock];
    isFaceTouch = true;
    faceTouched = NO;
    [faceDataLock unlock];
}


-(void)dealloc{
    
    for (int i=0; i<MAX_FACES; ++i) {
        [trackingRects[i] release];
    }
    [camera release];
    [super dealloc];
    [faceDataLock release], faceDataLock = NULL;
    
    [nameDataLock lock];
    for (int i=0; i<MAX_FACES; ++i) delete [] names[i];
    [nameDataLock unlock];
    [nameDataLock release], nameDataLock = NULL;
    
    [enteredNameLock release], enteredNameLock = NULL;
    [idOfTouchedFaceLock release], idOfTouchedFaceLock = NULL;
    [faceTouchLock release], faceTouchLock = NULL;
    
    if (enteredName) delete [] enteredName;
    //[uiView removeFromSuperview];
    //NSLog(@"FaceProcessView : dealloc");

}
-(void)destroy{
    _closing = YES;
    _pauseProcess = YES;
    
    int i = 1;
    while (_processingImage) {
        //wait while processing Image in FaceSDK, but not more than 2 seconds
        if (i++ > 20) break;
        [NSThread sleepForTimeInterval:0.1];
    }
}

-(void)cameraHasConnected{
#if defined(DEBUG)
    NSLog(@"Connected to camera");
#endif
}
-(void)drawFrame{
    // standart square
    static const GLfloat squareVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f,  1.0f,
    };
    //static UIInterfaceOrientation old_orientation = (UIInterfaceOrientation)0;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation != old_orientation) {
        old_orientation = orientation;
        
        [self relocateSubviewsForOrientation:orientation];
        //NSLog(@"Old  = %ld, statusBar = %ld",(long)old_orientation,(long)orientation);
    }
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, videoFrameTexture);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, squareVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    if (orientation == 0 || orientation == UIInterfaceOrientationPortrait) {
        GLfloat textureVertices[] = {
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
        };
        glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, 0, 0, textureVertices);
    } else if(orientation == UIInterfaceOrientationPortraitUpsideDown) {
        GLfloat textureVertices[] = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
        };
        glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, 0, 0, textureVertices);
    } else if(orientation == UIInterfaceOrientationLandscapeLeft) {
        GLfloat textureVertices[] = {
            1.0f, 1.0f,
            0.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
        };
        glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, 0, 0, textureVertices);
    } else if(orientation == UIInterfaceOrientationLandscapeRight) {
        GLfloat textureVertices[] = {
            0.0f, 0.0f,
            1.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
        };
        glVertexAttribPointer(ATTRIB_TEXTUREPOSITON, 2, GL_FLOAT, 0, 0, textureVertices);
    }
    glEnableVertexAttribArray(ATTRIB_TEXTUREPOSITON);
    [nameDataLock lock];
    /*for (int i=0; i<MAX_FACES; ++i) {
        if (strlen(names[i])) {
            [nameLabels[i] setString:[NSString stringWithUTF8String:names[i]]];
            [nameLabels[i] setForegroundColor:[[UIColor blueColor] CGColor]];
        } else {
            [nameLabels[i] setString:nil];
            //[nameLabels[i] setForegroundColor:[[UIColor greenColor] CGColor]];
        }
    }*/
    [nameDataLock unlock];
    
    [faceDataLock lock];
    for (int i=0; i<MAX_FACES; ++i) {
        if (faces[i].x2) { // have face
            //[nameLabels[i] setFrame:CGRectMake(10.0f, faces[i].y2 - faces[i].y1 + 10.0f, faces[i].x2-faces[i].x1-20.0f, 40.0f)];
            [nameLabels[i] setFrame:CGRectMake(10.0f, -40.0f, faces[i].x2-faces[i].x1-20.0f, 40.0f)];
            trackingRects[i].position = CGPointMake(faces[i].x1, faces[i].y1);
            trackingRects[i].bounds = CGRectMake(0.0f, 0.0f, faces[i].x2-faces[i].x1, faces[i].y2 - faces[i].y1);
            trackingRects[i].opacity = 1.0f;
        } else { // no face
            trackingRects[i].opacity = 0.0f;
        }
    }
    [faceDataLock unlock];
    
    [_glView setDisplayFramebuffer];
    glUseProgram(directDisplayProgram);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    [_glView presentFramebuffer];
    
    videoStarted = 1;
}

-(void)setDetectSquareColor:(UIColor*)color{
    [faceDataLock lock];
    trackingRects[0].borderColor = [color CGColor];
    [faceDataLock unlock];
}
- (void)showDetectLabel:(NSString*)label textColor:(UIColor*)color{
    /*if ([[nameLabels[0] string]isEqualToString:label] &&
        [nameLabels[0] foregroundColor] == [color CGColor]){
        return;
    }*/
        
    [nameDataLock lock];
    [nameLabels[0] setString:label];
    [nameLabels[0] setForegroundColor:[color CGColor]];
    [nameDataLock unlock];
}


-(void)processNewCameraFrame:(CVImageBufferRef)cameraFrame{
    if (rotating || _closing == YES)
        return;
    
    CVPixelBufferLockBaseAddress(cameraFrame, 0);
    int bufferHeight = (int)CVPixelBufferGetHeight(cameraFrame);
    int bufferWidth = (int)CVPixelBufferGetWidth(cameraFrame);
    
    // Create a new texture from the camera frame data, draw it (calling drawFrame)
    glGenTextures(1, &videoFrameTexture);
    glBindTexture(GL_TEXTURE_2D, videoFrameTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // This is necessary for non-power-of-two textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // Using BGRA extension to pull in video frame data directly
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bufferWidth, bufferHeight, 0, GL_BGRA, GL_UNSIGNED_BYTE, CVPixelBufferGetBaseAddress(cameraFrame));
    [self drawFrame];
    
    if (_processingImage == NO && faceTouched == NO && _pauseProcess == NO) {
        if (_closing) return;
        _processingImage = YES;
        
        // Copy camera frame to buffer
        
        int scanline = (int)CVPixelBufferGetBytesPerRow(cameraFrame);
        unsigned char * buffer = (unsigned char *)malloc(scanline * bufferHeight);
        if (buffer) {
            memcpy(buffer, CVPixelBufferGetBaseAddress(cameraFrame), scanline * bufferHeight);
        } else {
            _processingImage = NO;
            glDeleteTextures(1, &videoFrameTexture);
            CVPixelBufferUnlockBaseAddress(cameraFrame, 0);
            return;
        }
        
        // Execute face detection and recognition asynchronously
        
        DetectFaceParams args;
        args.width = bufferWidth;
        args.height = bufferHeight;
        args.scanline = scanline;
        args.buffer = buffer;
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (orientation == 0 || orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
            //args.ratio = (float)self.view.bounds.size.height/(float)bufferWidth;
            //using _glView size proportional to video size:
            args.ratio = (float)uiView.bounds.size.width/(float)bufferHeight;
        } else {
            //args.ratio = (float)self.view.bounds.size.width/(float)bufferWidth;
            //using _glView size proportional to video size:
            args.ratio = (float)uiView.bounds.size.height/(float)bufferHeight;
        }
        NSData * argsobj = [NSData dataWithBytes:&args length:sizeof(DetectFaceParams)];
        // will free (buffer) inside
        [self performSelectorInBackground:@selector(processImageAsyncWith:) withObject:argsobj];
    }
    if(_pauseProcess == YES){
        memset(faces, 0, sizeof(FaceRectangle)*MAX_FACES);
    }
    
    
    glDeleteTextures(1, &videoFrameTexture);
    CVPixelBufferUnlockBaseAddress(cameraFrame, 0);
}



-(BOOL) loadVertexShader:(NSString *)vertexShaderName fragmentShader:(NSString *)fragmentShaderName forProgram:(GLuint *)programPointer{
    GLuint vertexShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    *programPointer = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [vertexShaderName stringByAppendingString:@".vsh"];
    vertShaderPathname = [PodAsset pathForFilename:vertShaderPathname pod:@"CscRecognize"];
    //vertShaderPathname = [[NSBundle mainBundle] pathForResource:vertexShaderName ofType:@"vsh"];
    if (![self compileShader:&vertexShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
#if defined(DEBUG)
        NSLog(@"Failed to compile vertex shader");
#endif
        return FALSE;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [fragmentShaderName stringByAppendingString:@".fsh"];
    fragShaderPathname = [PodAsset pathForFilename:fragShaderPathname pod:@"CscRecognize"];
    //fragShaderPathname = [[NSBundle mainBundle] pathForResource:fragmentShaderName ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
#if defined(DEBUG)
        NSLog(@"Failed to compile fragment shader");
#endif
        return FALSE;
    }
    
    // Attach vertex shader to program.
    glAttachShader(*programPointer, vertexShader);
    
    // Attach fragment shader to program.
    glAttachShader(*programPointer, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(*programPointer, ATTRIB_VERTEX, "position");
    glBindAttribLocation(*programPointer, ATTRIB_TEXTUREPOSITON, "inputTextureCoordinate");
    
    // Link program.
    if (![self linkProgram:*programPointer]) {
#if defined(DEBUG)
        NSLog(@"Failed to link program: %d", *programPointer);
#endif
        // cleaning up
        if (vertexShader) {
            glDeleteShader(vertexShader);
            vertexShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (*programPointer) {
            glDeleteProgram(*programPointer);
            *programPointer = 0;
        }
        return FALSE;
    }
    
    // Release vertex and fragment shaders.
    if (vertexShader) {
        glDeleteShader(vertexShader);
    }
    if (fragShader) {
        glDeleteShader(fragShader);
    }
    return TRUE;
}
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    const GLchar * source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
#if defined(DEBUG)
        NSLog(@"Failed to load vertex shader");
#endif
        return FALSE;
    }
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    GLint status;
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return FALSE;
    }
    return TRUE;
}
-(BOOL) linkProgram:(GLuint)prog{
    glLinkProgram(prog);
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    GLint status;
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0)
        return FALSE;
    return TRUE;
}

- (BOOL)validateProgram:(GLuint)prog
{
    glValidateProgram(prog);
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
#endif
    GLint status;
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0)
        return FALSE;
    return TRUE;
}

- (CGSize)screenSizeOrientationIndependent {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    return CGSizeMake(MIN(screenSize.width, screenSize.height), MAX(screenSize.width, screenSize.height));
}
-(UIImage *) getUIImageFromBuffer:(HImage) hImage faceParam:(DetectFaceParams)faceParam{
    
    NSString * imagePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.jpg"];
    FSDK_SaveImageToFile(hImage, (char *)[imagePath UTF8String]);
#if defined(DEBUG)
    //NSLog(@"saved to %s with %d", [imagePath UTF8String], res);
#endif
    UIImage * cocoa_image = [UIImage imageWithContentsOfFile:imagePath];
    //UIImageWriteToSavedPhotosAlbum(cocoa_image, nil, nil, nil);
    
    return cocoa_image;
}

-(void)relocateSubviewsForOrientation:(UIInterfaceOrientation)orientation{
    NSLog(@"relocateSubviewsForOrientation");
    [_glView destroyFramebuffer];
    [_glView removeFromSuperview]; //XXX: does not call [_glView release] immediately on iOS9!
    
    //CGRect applicationFrame = [screenForDisplay applicationFrame];
    CGSize applicationFrame = [self screenSizeOrientationIndependent]; //workaround iOS 8 change, that sizes become orientation-dependent
    
    //DEBUG
    //const int video_width = 352;
    //const int video_height = 288;
    
    const int video_width = (int)camera.width; //640;
    const int video_height = (int)camera.height;//480;
    if (orientation == 0 || orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //_glView = [[RecognitionGLView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, applicationFrame.width, applicationFrame.height)];
        //using _glView size proportional to video size:
        _glView = [[RecognitionGLView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, applicationFrame.width, applicationFrame.width * (video_width*1.0f/video_height))];
    } else {
        //_glView = [[RecognitionGLView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, applicationFrame.height, applicationFrame.width)];
        //using _glView size proportional to video size:
        _glView = [[RecognitionGLView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, applicationFrame.width * (video_width*1.0f/video_height), applicationFrame.width)];
    }
    [uiView addSubview:_glView];
    [_glView release]; //now self.view is responsible for the view
    [self loadVertexShader:@"DirectDisplayShader" fragmentShader:@"DirectDisplayShader" forProgram:&directDisplayProgram];
    for (int i=0; i<MAX_FACES; ++i) {
        [_glView.layer addSublayer:trackingRects[i]];
    }
    
    [uiView sendSubviewToBack:_glView];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (![delegate allowFaceTouch]){
        return;
    }
    currentTouchPoint = [[touches anyObject] locationInView:self];
    int x = currentTouchPoint.x;
    int y = currentTouchPoint.y;
    
    [idOfTouchedFaceLock lock];
    idOfTouchedFace = -1;
    [idOfTouchedFaceLock unlock];
    
    [faceDataLock lock];
    for (int i=0; i<MAX_FACES; ++i) {
        if (PointInRectangle(x, y, faces[i].x1, faces[i].y1, faces[i].x2, faces[i].y2 + 30)) {
            indexOfTouchedFace = i;
            [idOfTouchedFaceLock lock];
            idOfTouchedFace = IDs[i];
            [idOfTouchedFaceLock unlock];
            break;
        }
    }
    [faceDataLock unlock];
    [idOfTouchedFaceLock lock];
    if (idOfTouchedFace != -1) {
        [idOfTouchedFaceLock unlock];
        faceTouched = YES;
        [faceDataLock lock];
        isFaceTouch = true;
        faceTouched = NO;
        [faceDataLock unlock];
    } else {
        [idOfTouchedFaceLock unlock];
    }
#if defined(DEBUG)
    NSLog(@"touch at (%d, %d)", x, y);
#endif
}

-(void)processImageAsyncWith:(NSData *)args{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //required for async execution
    // Do not forget to [pool release] on exit!
    
    if (_closing) {
        [pool release];
        return;
    }
    
    // Cleaning tracker memory, if the button was pressed
    
    if (clearTracker) {
        FSDK_ClearTracker(_tracker);
        [self resetTrackerParameters];
        clearTracker = NO;
    }
    
    
    // Reading buffer parameters
    
    DetectFaceParams a;
    [args getBytes:&a length:sizeof(DetectFaceParams)];
    unsigned char * buffer = a.buffer;
    int width = a.width;
    int height = a.height;
    int scanline = a.scanline;
    float ratio = a.ratio;
    
    if(isFaceTouch == true){
        //[faceDataLock lock];
        [delegate onFaceTouched:currentHImage faceParam:a];
        FSDK_FreeImage(currentHImage);
        isFaceTouch = false;
        faceTouched = NO;
        _processingImage = NO;
        //_pauseProcess = NO;
        //[faceDataLock unlock];
        return;
    }

    
    // Converting BGRA to RGBA
    
    unsigned char * p1line = buffer;
    unsigned char * p2line = buffer+2;
    for (int y=0; y<height; ++y) {
        unsigned char * p1 = p1line;
        unsigned char * p2 = p2line;
        p1line += scanline;
        p2line += scanline;
        for (int x=0; x<width; ++x) {
            unsigned char tmp = *p1;
            *p1 = *p2;
            *p2 = tmp;
            p1 += 4;
            p2 += 4;
        }
    }

    HImage image;
    int res = FSDK_LoadImageFromBuffer(&image, buffer, width, height, scanline, FSDK_IMAGE_COLOR_32BIT);
    free(buffer);
    if (res != FSDKE_OK) {
#if defined(DEBUG)
        NSLog(@"FSDK_LoadImageFromBuffer failed with %d", res);
#endif
        [pool release];
        _processingImage = NO;
        return;
    }
    
    // Rotating image basing on orientation
    
    HImage derotated_image;
    res = FSDK_CreateEmptyImage(&derotated_image);
    FSDK_CreateEmptyImage(&currentHImage);
    
    if (res != FSDKE_OK) {
#if defined(DEBUG)
        NSLog(@"FSDK_CreateEmptyImage failed with %d", res);
#endif
        FSDK_FreeImage(image);
        [pool release];
        _processingImage = NO;
        return;
    }
    UIInterfaceOrientation df_orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (df_orientation == 0 || df_orientation == UIInterfaceOrientationPortrait) {
        res = FSDK_RotateImage90(image, 1, derotated_image);
    } else if (df_orientation == UIInterfaceOrientationPortraitUpsideDown) {
        res = FSDK_RotateImage90(image, -1, derotated_image);
    } else if (df_orientation == UIInterfaceOrientationLandscapeLeft) {
        res = FSDK_RotateImage90(image, 0, derotated_image); //will simply copy image
    } else if (df_orientation == UIInterfaceOrientationLandscapeRight) {
        res = FSDK_RotateImage90(image, 2, derotated_image);
    }
    if (res != FSDKE_OK) {
#if defined(DEBUG)
        NSLog(@"FSDK_RotateImage90 failed with %d", res);
#endif
        FSDK_FreeImage(image);
        FSDK_FreeImage(derotated_image);
        FSDK_FreeImage(currentHImage);
        [pool release];
        _processingImage = NO;
        return;
    }
    
    res = FSDK_MirrorImage(derotated_image, true);
    if (res != FSDKE_OK) {
#if defined(DEBUG)
        NSLog(@"FSDK_MirrorImage failed with %d", res);
#endif
        FSDK_FreeImage(image);
        FSDK_FreeImage(derotated_image);
        FSDK_FreeImage(currentHImage);
        [pool release];
        _processingImage = NO;
        return;
    }
    
    // Passing frame to FaceSDK, reading face coordinates and names
    FSDK_CopyImage(derotated_image, currentHImage);
    FSDK_Features Eyes;
    
    int result = FSDK_DetectFacialFeatures(derotated_image, &Eyes);
    memset(faces, 0, sizeof(FaceRectangle)*MAX_FACES);
    if(result == FSDKE_OK){
        memset(names[0], 0, MAX_NAME_LEN + 1);
        
        [faceDataLock lock];
        [self getFaceFrame:&Eyes x1:&(faces[0].x1) y1:&(faces[0].y1) x2:&(faces[0].x2) y2:&(faces[0].y2)];
        faces[0].x1 *= ratio;
        faces[0].x2 *= ratio;
        faces[0].y1 *= ratio;
        faces[0].y2 *= ratio;
        [faceDataLock unlock];
        
        [delegate onFaceDetected:derotated_image FacialFeatures:&Eyes];
        //[self getEyeOpenPercent:derotated_image features:&Eyes];
    }else{
        [delegate onFaceNotDetected];
    }
    if(result != FSDKE_OK && waitForEyeBlink){
        [self stopBlinkEyeTimer];
    }
    
    
    FSDK_FreeImage(image);
    FSDK_FreeImage(derotated_image);
    if(isFaceTouch == false) FSDK_FreeImage(currentHImage);
    [pool release];
    _processingImage = NO;
}

-(int) getEyeOpenPercent:(HImage)hImage features:(FSDK_Features*)features{
    float EyeOpen = 0.0f;
    char AttributeValues[1024];
    FSDK_DetectFacialAttributeUsingFeatures(hImage, features,"Expression", AttributeValues, sizeof(AttributeValues));
    FSDK_GetValueConfidence(AttributeValues, "EyesOpen",&EyeOpen);
    return EyeOpen * 100;
}

- (void) checkBlinkEye:(HImage)hImage
              features:(FSDK_Features*)features
            onEyeOpened:( BOOL(^)(HImage,FSDK_Features*) )onEyeOpened
   onBlinkEyeCompleted:( void(^)(void) )onBlinkEyeCompleted{
    if(![eyeBlinkDelegate enableBlinkEye]) return;
    
    if(!waitForEyeBlink){
        int openEyePercent = [self getEyeOpenPercent:hImage features:features];
        if(openEyePercent > MIN_EYE_OPEN_PERCENT ){
            if (onEyeOpened(hImage,features)) [self startBlinkEyeTimer];
        }else{
            NSString* openEyeMsg = eyeBlinkDelegate!=nil?[eyeBlinkDelegate getOpenEyeLabel]:DEFAULT_OPEN_EYE_LABEL;
            [self showDetectLabel:openEyeMsg textColor:[UIColor greenColor]];
            [self setDetectSquareColor:UIColor.blueColor];
        }
    }
    
    if(waitForEyeBlink){
        int openEyePercent = [self getEyeOpenPercent:hImage features:features];
        if(openEyePercent > MIN_EYE_BLINK_PRECENT) {
            NSString* blinkEyeMsg = eyeBlinkDelegate!=nil?[eyeBlinkDelegate getBlinkEyeLabel]:DEFAULT_BLINK_EYE_LABEL;
            [self showDetectLabel:blinkEyeMsg textColor:[UIColor greenColor]];
            [self setDetectSquareColor:UIColor.whiteColor];
            return;
        };
        
        [self stopBlinkEyeTimer];
        onBlinkEyeCompleted();
    }
}
-(void) startBlinkEyeTimer{
    waitForEyeBlink = TRUE;
    dispatch_async(dispatch_get_main_queue(), ^{
        int timeout = eyeBlinkDelegate!=nil?[eyeBlinkDelegate blinkEyeTimeout]:DEFAULT_BLINK_EYE_TIMEOUT_SECOND;
        
        blinkEyeTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target: self
                                                       selector:@selector(onBlinkEyeTimerFinish:)
                                                       userInfo:nil
                                                        repeats:NO];
    });
    
}

-(void) stopBlinkEyeTimer{
    if(blinkEyeTimer != nil) {
        [blinkEyeTimer invalidate];
        blinkEyeTimer = nil;
        waitForEyeBlink = FALSE;
    }
}

-(void)onBlinkEyeTimerFinish:(NSTimer *)timer {
    waitForEyeBlink = FALSE;
}


@end
