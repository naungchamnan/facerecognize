//
//  FaceProcessView.h
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecognitionCamera.h"
#import "RecognitionGLView.h"
#include "LuxandFaceSDK.h"

#define MAX_FACES 1
#define DEBUG 1
#define MAX_NAME_LEN 1024
#define MIN_EYE_OPEN_PERCENT 70
#define MIN_EYE_BLINK_PRECENT 20
#define DEFAULT_BLINK_EYE_TIMEOUT_SECOND 5
#define DEFAULT_BLINK_EYE_LABEL @"Please blink your eyes"
#define DEFAULT_OPEN_EYE_LABEL @"Please open your eyes"

typedef struct {
    unsigned char * buffer;
    int width, height, scanline;
    float ratio;
} DetectFaceParams;

typedef struct {
    int x1, x2, y1, y2;
} FaceRectangle;

typedef NS_ENUM(NSInteger,FaceCompareType) {
    FaceCompareType_EYES,FaceCompareType_FACIAL,FaceCompareType_BOTH
};

typedef NS_ENUM(NSInteger,TemplateViewType){
    TemplateViewType_MEMBER,TemplateViewType_EMPLOYEE
};

@protocol TemplateViewModel
- (NSString *) getId;
- (NSString *) getName;
- (NSMutableArray *) getEyeTemplates;
- (NSMutableArray *) getFacialTemplates;
@end

@protocol FaceRecognitionCriteria
- (float) similarity;
- (BOOL) findAnotherSameFace;
- (FaceCompareType) getCompareType;
- (NSDictionary*) getTemplateViewModelDictionary;
//- (NSDictionary*) getFacialTemplateDic;
@end

@protocol EyeBlinkDelegate
- (NSString*) getOpenEyeLabel;
- (NSString*) getBlinkEyeLabel;
- (int) blinkEyeTimeout;
- (BOOL) enableBlinkEye;
@end


@protocol FaceProcessViewDelegate
- (void) onFaceDetected:(HImage)hImage FacialFeatures:(FSDK_Features *) FacialFeatures;
- (void) onFaceNotDetected;
- (void) onFaceTouched:(HImage)hImage faceParam:(DetectFaceParams)faceParam;
- (void) onPauseProcessView;
- (BOOL) allowFaceTouch;
@end

@interface FaceProcessView : UIView<RecognitionCameraDelegate>
{
    UIView * uiView;
    RecognitionCamera * camera;
    GLuint directDisplayProgram;
    GLuint videoFrameTexture;
    GLubyte * rawPositionPixels;
    
    NSLock * enteredNameLock;
    NSLock * faceTouchLock;
    char * enteredName;
    volatile int namedFaceID;
    HImage currentHImage;
    
    CALayer * trackingRects[MAX_FACES];
    CATextLayer * nameLabels[MAX_FACES];
    
    //volatile int processingImage;
    
    NSLock * faceDataLock;    FaceRectangle faces[MAX_FACES];
    NSLock * nameDataLock;
    char * names[MAX_FACES];
    long long IDs[MAX_FACES];
    volatile int faceTouched;
    volatile int indexOfTouchedFace;
    NSLock * idOfTouchedFaceLock;
    long long idOfTouchedFace;
    BOOL isFaceTouch;
    CGPoint currentTouchPoint;
    
    volatile int rotating;
    volatile BOOL waitForEyeBlink;
    volatile NSTimer* blinkEyeTimer;
    char videoStarted;
    
    volatile int clearTracker;
}

@property(readonly) RecognitionGLView * glView;
@property(readonly) HTracker tracker;
@property(readwrite) char * templatePath;
@property(readwrite) volatile int closing;
@property(readonly) volatile int processingImage;
@property(readwrite) volatile int pauseProcess;
@property(nonatomic, assign) id<FaceProcessViewDelegate> delegate;
@property(nonatomic, assign) id<EyeBlinkDelegate> eyeBlinkDelegate;

- (void)initView:(UIView *)parentUIView;
- (int) getFaceFrame:(const FSDK_Features *)Features x1:(int *)x1 y1:(int *)y1 x2:(int *)x2 y2:(int *)y2;
- (UIImage *) getUIImageFromBuffer:(HImage) hImage faceParam:(DetectFaceParams)faceParam;

// OpenGL ES 2.0 setup methods
- (BOOL)loadVertexShader:(NSString *)vertexShaderName fragmentShader:(NSString *)fragmentShaderName forProgram:(GLuint *)programPointer;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
- (void)destroy;
- (void) stopCamera;
- (void) startCamera;
- (void) simulateFaceTouch;

// Device rotating support
- (void)relocateSubviewsForOrientation:(UIInterfaceOrientation)orientation;

// Image processing in FaceSDK
- (void)processImageAsyncWith:(NSData *)args;
- (void)setDetectSquareColor:(UIColor*)color;
- (void)showDetectLabel:(NSString*)label textColor:(UIColor*)color;

// Eye Blink
- (int) getEyeOpenPercent:(HImage)hImage features:(FSDK_Features*)features;
- (void) checkBlinkEye:(HImage)hImage features:(FSDK_Features*)features onEyeOpened:( BOOL(^)(HImage,FSDK_Features*) )onEyeOpened onBlinkEyeCompleted:( void(^)(void) )onBlinkEyeCompleted;
@end

