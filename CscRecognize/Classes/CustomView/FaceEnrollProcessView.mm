//
//  FaceEnrollProcessView.m
//  SF2
//
//  Created by Pongpop Inkaew on 6/7/2559 BE.
//  Copyright © 2559 Computer Science. All rights reserved.
//

#import "FaceEnrollProcessView.h"

@interface FaceEnrollTemp : NSObject

@property(strong,atomic) NSData *captureImage;
@property(strong,atomic) NSData *eyesTemplate;
@property(strong,atomic) NSData *facialTempalte;
@property(atomic) NSInteger enrollCount;

@end

@implementation FaceEnrollTemp
-(void)dealloc{
    NSLog(@"FaceEnrollTemp : dealloc %ld",(long)[self enrollCount]);
    [super dealloc];
}
@end


@implementation FaceEnrollProcessView

@synthesize viewDelegate;
@synthesize enrollBarView;

-(void)initView:(UIView *)parentUIView{
    self.delegate = self;
    arrayLock = [[NSLock alloc] init];
    enrollModel = [[[FaceEnrollViewModel alloc]init]autorelease];
    faceConstArrayTemp = [[NSArray alloc]init];
    
    self.faceEnrollTempArray = [[NSMutableArray alloc]init];
    [super initView:parentUIView];
    [self startEnroll];
    whiteColor = [UIColor whiteColor];
    blueColor = [UIColor blueColor];
    
}

-(void)dealloc{
    [arrayLock release]; arrayLock = NULL;
    
    [_faceEnrollTempArray removeAllObjects];
    [_faceEnrollTempArray release]; _faceEnrollTempArray = NULL;
    //[faceConstArrayTemp release];
    faceConstArrayTemp = nil;
    enrollCountNo = 0;
    _criteria = NULL;
    [super dealloc];
}

-(void)startEnroll{
    [super setPauseProcess:NO];
}

-(void)pauseEnroll{
    [super setPauseProcess:YES];
}

-(void)resetView{
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewDelegate onFaceEnrollResetView];
        [enrollBarView resetView];
    });
    [_faceEnrollTempArray removeAllObjects];
    [_faceEnrollTempArray release];
    faceConstArrayTemp = nil;
    
    faceConstArrayTemp = [[NSArray alloc]init];
    self.faceEnrollTempArray = [[NSMutableArray alloc]init];
    enrollModel = [[[FaceEnrollViewModel alloc]init]autorelease];
    enrollCountNo = 0;
}


-(void)onFaceTouched:(HImage)hImage faceParam:(DetectFaceParams)faceParam{
    // This method call in background thread
#if defined(DEBUG)
    NSLog(@"onFaceTouched");
#endif
    if(viewDelegate == nil || _criteria == nil)
        return;
    
    FSDK_FaceTemplate eyesTemplate = FSDK_FaceTemplate();
    FSDK_FaceTemplate facialTemplate = FSDK_FaceTemplate();
    FSDK_Features FacaialFeature;

    if (FSDKE_OK == FSDK_DetectFacialFeatures(hImage, &FacaialFeature)
        && FSDK_GetFaceTemplateUsingEyes(hImage, &FacaialFeature, &eyesTemplate) == FSDKE_OK
        && FSDK_GetFaceTemplateUsingFeatures(hImage, &FacaialFeature, &facialTemplate) == FSDKE_OK) {
        @try {
            
            enrollCountNo +=1 ;
            UIImage *image = [super getUIImageFromBuffer:hImage faceParam:faceParam];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [viewDelegate onFaceEnrollCaptureFaceImage:image countNo:enrollCountNo];
                [enrollBarView showCaptureImage:image countNo:enrollCountNo];
            });
            
            [faceDataLock lock];
            NSData * eyesTemplateData = [NSData dataWithBytes:&eyesTemplate length:sizeof(FSDK_FaceTemplate)];
            NSData * facialTemplateData = [NSData dataWithBytes:&facialTemplate length:sizeof(FSDK_FaceTemplate)];
            [faceDataLock unlock];
            
            NSData * imageData = UIImageJPEGRepresentation(image, 1);
            [arrayLock lock];
            @synchronized (_faceEnrollTempArray) {
                FaceEnrollTemp * enrollTemp = [[[FaceEnrollTemp alloc]init]autorelease];
                enrollTemp.captureImage = imageData;
                enrollTemp.eyesTemplate = eyesTemplateData;
                enrollTemp.facialTempalte = facialTemplateData;
                enrollTemp.enrollCount = enrollCountNo;
                [_faceEnrollTempArray addObject:enrollTemp];
            }
            [arrayLock unlock];
            
            if(enrollCountNo > 1 && ![self isMatchFace]){
                [self resetView];
                [self pauseEnroll];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [viewDelegate onFaceEnrollNotMatch];
                 });
                return;
            }
            if(enrollCountNo == MAX_ENROLL_COUNT_NO){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self pauseEnroll];
                    faceConstArrayTemp = [_faceEnrollTempArray copy];
                    FaceEnrollViewModel * faceEnrollViewModel = [[[FaceEnrollViewModel alloc]init] autorelease];
                    
                    faceEnrollViewModel.captureImage1 = ((FaceEnrollTemp*)faceConstArrayTemp[0]).captureImage;
                    faceEnrollViewModel.captureImage2 = ((FaceEnrollTemp*)faceConstArrayTemp[1]).captureImage;
                    faceEnrollViewModel.captureImage3 = ((FaceEnrollTemp*)faceConstArrayTemp[2]).captureImage;
                    faceEnrollViewModel.eyesTemplate = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:0]).eyesTemplate;
                    faceEnrollViewModel.eyesTemplate2 = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:1]).eyesTemplate;
                    faceEnrollViewModel.eyesTemplate3 = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:2]).eyesTemplate;
                    
                    faceEnrollViewModel.facialTempalte = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:0]).facialTempalte;
                    faceEnrollViewModel.facialTempalte2 = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:1]).facialTempalte;
                    faceEnrollViewModel.facialTempalte3 = ((FaceEnrollTemp*)[faceConstArrayTemp objectAtIndex:2]).facialTempalte;
                    [faceConstArrayTemp release];
                    [viewDelegate onFaceEnrollSuccess:faceEnrollViewModel];
                });
            }
            
            
        } @catch (NSException *exception) {
             NSLog(@"%@", exception.reason);
        } @finally {
            
        }
        
    }
    
}

-(BOOL) isMatchFace{
    faceConstArrayTemp = [_faceEnrollTempArray copy];
    NSUInteger enrollSize = faceConstArrayTemp.count - 1;
    
    FaceEnrollTemp *tmp1 = [faceConstArrayTemp objectAtIndex:0];
    FSDK_FaceTemplate faceTemplate1;
    [tmp1.eyesTemplate getBytes:&faceTemplate1 length:sizeof(FSDK_FaceTemplate)];
    [tmp1 release];
    
    for (int enrollIndex = 0 ; enrollIndex < enrollSize; enrollIndex++) {
        float similarity = 0;
        
        FaceEnrollTemp *tmp2 = [faceConstArrayTemp objectAtIndex:(enrollIndex+1)];
        FSDK_FaceTemplate faceTemplate2 = FSDK_FaceTemplate();
        
        [tmp2.eyesTemplate getBytes:&faceTemplate2 length:sizeof(FSDK_FaceTemplate)];
        [tmp2 release];
        FSDK_MatchFaces(&faceTemplate1, &faceTemplate2, &similarity);
        NSLog(@"similarity = %f, criteria-similarity = %f",similarity,[_criteria similarity]);
        if(similarity < [_criteria similarity]){
            return false;
        }
    }
    
    return true;
}

-(void)onFaceDetected:(HImage)hImage FacialFeatures:(FSDK_Features *)FacialFeatures{
    int _tmpCount = enrollCountNo+1;
    [enrollBarView showTouchView:_tmpCount];
}

-(void)onFaceNotDetected{
    int _tmpCount = enrollCountNo+1;
    [enrollBarView hideTouchView:_tmpCount];
}

-(BOOL) allowFaceTouch{
    return enrollBarView == nil;
}

-(void)onPauseProcessView{
    
}

@end
