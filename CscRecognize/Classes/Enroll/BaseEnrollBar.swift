//
//  SampleEnrollBar.swift
//  Pods
//
//  Created by Pongpop Inkaew on 6/24/2560 BE.
//
//

import UIKit

public protocol EnrollBarViewDelegate : class{
    func getTouchView()->[Int:UIView]
    func resetView();
    func showCaptureImageView(_ image: UIImage!,_ countNo:Int)
}

protocol FaceEnrollViewTouchDelegate : class{
    func onViewTouch(_ captureNo:Int)
}

open class MyFaceEnrollBarManager:NSObject,FaceEnrollBarManager,UIGestureRecognizerDelegate {
    fileprivate let ALPHA_INVISIBLE = CGFloat(0.15)
    fileprivate let ALPHA_VISIBLE = CGFloat(1.00)
    var touchViewDict:[Int:UIView] = [:]
    var currentCaptureNo:Int = 0
    weak var viewTouchDelegate:FaceEnrollViewTouchDelegate?
    weak var delegate:EnrollBarViewDelegate?
    
    override init() {
        super.init()
    }
    
    func prepareView(){
        touchViewDict = delegate?.getTouchView() ?? [:]
        touchViewDict.forEach{ ele in
            ele.value.alpha = ALPHA_INVISIBLE
            ele.value.gestureRecognizers?.forEach{ ele.value.removeGestureRecognizer($0) }
        }
    }
    
    public func showTouchView(_ countNo: Int32) {
        currentCaptureNo = Int(countNo)
        guard let view = touchViewDict[currentCaptureNo]  else{
            return
        }
        if view.alpha == ALPHA_VISIBLE{ return }
       
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.onTouchView(_:)))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(gesture)
        view.isUserInteractionEnabled = true
        view.alpha = ALPHA_VISIBLE
    }
    
    @objc func onTouchView(_ sender:UITapGestureRecognizer){
        viewTouchDelegate?.onViewTouch(currentCaptureNo)
    }
    
    public func hideTouchView(_ countNo: Int32) {
        guard let view = touchViewDict[Int(countNo)] else{
            return
        }
        if view.alpha == ALPHA_INVISIBLE { return }
        
        view.alpha = ALPHA_INVISIBLE
        guard let gestures = view.gestureRecognizers, gestures.count > 0 else { return }
        view.removeGestureRecognizer(gestures[0])
    }
    
    public func resetView() {
        delegate?.resetView()
        prepareView()
    }
    
    public func showCapture(_ image: UIImage!, countNo: Int32) {
        hideTouchView(countNo)
        delegate?.showCaptureImageView(image,Int(countNo))
    }
}
