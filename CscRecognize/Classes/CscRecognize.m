//
//  CscRecognize.m
//  Pods
//
//  Created by Pongpop Inkaew on 6/8/2560 BE.
//
//

#import "CscRecognize.h"
#include "LuxandFaceSDK.h"

@implementation CscRecognize
+ (void) activate:(NSString*)licenseKey{
    int res = FSDKE_OK;
    res = FSDK_ActivateLibrary([licenseKey UTF8String]);
    NSLog(@"activation result %d\n", res);
    
    if (res) exit(res);
    
    char licenseInfo[1024];
    FSDK_GetLicenseInfo(licenseInfo);
    NSLog(@"license: %s\n", licenseInfo);
    res = FSDK_Initialize((char *)"");
    
    if (res) exit(res);
    int threadcount = 0;
    res = FSDK_GetNumThreads(&threadcount);
    NSLog(@"thread count %d\n", threadcount);
}
@end
