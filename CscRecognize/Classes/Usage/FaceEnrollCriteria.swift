//
//  FaceEnrollCriteria.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 6/24/2560 BE.
//  Copyright © 2560 CocoaPods. All rights reserved.
//

import Foundation

open class FaceEnrollCriteria : FaceRecognitionCriteria{
    var templateViewModelDic:[NSString:TemplateViewModel]
    var similarityValue:Float = 0.85
    
   public init(){
        templateViewModelDic = [:]
    }
    
    public func similarity() -> Float {
        return similarityValue;
    }
    public func findAnotherSameFace() -> Bool {
        return false
    }
    public func getCompareType() -> FaceCompareType {
        return .EYES
    }
    public func getTemplateViewModelDictionary() -> [AnyHashable : Any]! {
        return templateViewModelDic
    }
    
}
