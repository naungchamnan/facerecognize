//
//  FaceIdentifierView.swift
//  Pods
//
//  Created by Pongpop Inkaew on 7/9/2560 BE.
//
//

import UIKit

open class FaceIdentifierView: UIView {
    fileprivate var processView:FaceIdentifierProcessView?
    open weak var delegate:FaceIdentifierProcessViewDelegate?{
        get{ return self.processView?.viewDelegate }
        set{ self.processView?.viewDelegate = newValue }
    }
    open weak var eyeBlinkDelegate:EyeBlinkDelegate?{
        get{ return self.processView?.eyeBlinkDelegate }
        set{ self.processView?.eyeBlinkDelegate = newValue }
    }
    open weak var criteria:FaceIdentifierCriteria?{
        get{ return self.processView?.criteria as? FaceIdentifierCriteria }
        set{ self.processView?.criteria = newValue }
    }
    
    open func prepare(_ preview:UIView) {
        processView = FaceIdentifierProcessView()
        processView?.initView(preview)
        self.layer.frame = preview.layer.frame
    }
    open func destroy(){
        processView?.destroy()
    }
    
    open func startIdentifier(){
        processView?.startIdentifier()
    }
    
    open func pauseIdentifier(){
        processView?.pauseIdentifier()
    }
    
    open func refresh(){
        processView?.refresh()
    }
    
    open func stopCamera(){
        processView?.stopCamera()
    }
    
    open func startCamera(){
        processView?.startCamera()
    }
}
