//
//  FaceIdentifierCriteria.swift
//  Pods
//
//  Created by Pongpop Inkaew on 7/9/2560 BE.
//
//

import Foundation

open class FaceIdentifierCriteria : FaceRecognitionCriteria{
    open var templateViewModelDic:[NSString:TemplateViewModel]
    open var similarityValue:Float = 0.90
    open var compareType:FaceCompareType = .EYES
    
    public init(){
        templateViewModelDic = [:]
    }
    
    public func similarity() -> Float {
        return similarityValue;
    }
    public func findAnotherSameFace() -> Bool {
        return false
    }
    public func getCompareType() -> FaceCompareType {
        return compareType
    }
    public func getTemplateViewModelDictionary() -> [AnyHashable : Any]! {
        return templateViewModelDic
    }
    
    public func addTemplateModel(key:String,value:TemplateViewModel){
        templateViewModelDic[key as NSString] = value
    }
}
