//
//  FaceEnrollView.swift
//  Pods
//
//  Created by Pongpop Inkaew on 6/24/2560 BE.
//
//

import Foundation

open class FaceEnrollView: UIView ,FaceEnrollViewTouchDelegate{
    fileprivate var mProcessView:FaceEnrollProcessView?
    fileprivate let _tmpCriteria:FaceEnrollCriteria = FaceEnrollCriteria()
    fileprivate var enrollbarManager:MyFaceEnrollBarManager?
    open weak var enrollBarViewDelegate:EnrollBarViewDelegate?{
        get{
            return self.enrollbarManager?.delegate
        }
        set{
            enrollbarManager?.delegate = newValue
            enrollbarManager?.prepareView()
        }
    }
    
    open var criteria:FaceEnrollCriteria?{
        get{ return self.mProcessView?.criteria as? FaceEnrollCriteria}
        set{ self.mProcessView?.criteria = newValue}
    }
    
    open weak var delegate:FaceEnrollProcessViewDelegate?{
        get{ return self.mProcessView?.viewDelegate }
        set{ self.mProcessView?.viewDelegate = newValue }
    }
    open weak var eyeBlinkDelegate:EyeBlinkDelegate?{
        get{ return self.mProcessView?.eyeBlinkDelegate }
        set{ self.mProcessView?.eyeBlinkDelegate = newValue }
    }
    
    open func prepare(_ parentView:UIView) {
        enrollbarManager = MyFaceEnrollBarManager()
        enrollbarManager?.viewTouchDelegate = self
        //criteria = FaceEnrollCriteria()
        mProcessView = FaceEnrollProcessView()
        mProcessView?.criteria = _tmpCriteria
        mProcessView?.initView(parentView)
        mProcessView?.enrollBarView = enrollbarManager
        
        self.layer.frame = parentView.layer.frame
    }
    open func destroy(){
        mProcessView?.destroy()
    }
    
    open func stopCamera(){
        mProcessView?.stopCamera()
    }
    
    open func startCamera(){
        mProcessView?.startCamera()
    }
    
    open func startEnroll(){
        mProcessView?.startEnroll()
    }
    
    open func resetView(){
        mProcessView?.resetView()
    }
    
    open func pauseEnroll(){
        mProcessView?.pauseEnroll()
    }
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        mProcessView?.touchesBegan(touches, with: event)
    }
    
    func onViewTouch(_ captureNo: Int) {
        mProcessView?.simulateFaceTouch()
    }
}
