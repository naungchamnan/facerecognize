#
# Be sure to run `pod lib lint CscRecognize.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CscRecognize'
  s.version          = '1.0.10'
  s.summary          = 'A short description of CscRecognize.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/naungchamnan/facerecognize'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pongpop Inkaew' => 'nice.comsci@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/naungchamnan/facerecognize.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'CscRecognize/Classes/**/*'
  s.requires_arc = false
  s.vendored_libraries = 'CscRecognize/Classes/FaceSDK/libfsdk-static.a', 'CscRecognize/Classes/FaceSDK/libfsdk-static_64.a'
  s.library = 'z'
  s.pod_target_xcconfig = { 'ENABLE_BITCODE' => 'NO' , 'OTHER_LDFLAGS' => '-Wl,-read_only_relocs,suppress,-lstdc++,-lc++' }

  s.resource_bundles = {
   'CscRecognize' => ['CscRecognize/Classes/Shaders/*.{fsh,vsh}']
  }
#s.resources = "CscRecognize/**/*.{fsh,vsh}"

  s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'PodAsset'
  # s.dependency 'AFNetworking', '~> 2.3'

  def s.post_install(target_installer)

  end
end
