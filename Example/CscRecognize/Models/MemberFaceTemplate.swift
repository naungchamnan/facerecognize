//
//  MemberFaceTemplate.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 7/10/2560 BE.
//  Copyright © 2560 CocoaPods. All rights reserved.
//

import Foundation
import CscRecognize

class MemberFaceTemplate : TemplateViewModel{
    var eyeTemplates:[Data] = []
    var facailTemplates:[Data] = []
    var id:String?
    
    func getId() -> String! {
        return id
    }
    func getName() -> String! {
        return ""
    }
    func getEyeTemplates() -> NSMutableArray! {
        //return eyeTemplates as! NSMutableArray
        return NSMutableArray(array: eyeTemplates)
    }
    func getFacialTemplates() -> NSMutableArray! {
        return facailTemplates as! NSMutableArray
    }
    
    func addEyeTemplate(_ data:Data){
        eyeTemplates.append(data)
    }
}
