//
//  FaceRecognizeViewController.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 7/9/2560 BE.
//  Copyright © 2560 CocoaPods. All rights reserved.
//

import UIKit
import CscRecognize

class FaceRecognizeViewController: UIViewController {
    @IBOutlet weak var cameraPreview: UIView!
    var faceIdentifierView:FaceIdentifierView?
    var criteria:FaceIdentifierCriteria?
    let model = MemberFaceTemplate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadExampleFile()
        criteria = FaceIdentifierCriteria()
        faceIdentifierView = FaceIdentifierView()
        faceIdentifierView?.prepare(cameraPreview)
        faceIdentifierView?.delegate = self
        //faceIdentifierView?.criteria = criteria
        faceIdentifierView?.eyeBlinkDelegate = self
        
    }
    
    func downloadExampleFile(){
        model.id = "M_1"
        HttpDownloader().downloadWith(url: "https://zm.csc.co.th///ZeemeWebApp/memberload?memberid=e31bfb10-19bb-11e7-81f7-e565e26a97f0&fileid=f96a2992-33de-11e7-aef2-f545ba5b73ef"){
            (data,error) in
            self.model.addEyeTemplate(data!)
            self.criteria?.addTemplateModel(key: "M_1", value: self.model)
            self.faceIdentifierView?.criteria = self.criteria
            self.faceIdentifierView?.refresh()
        }
        HttpDownloader().downloadWith(url: "https://zm.csc.co.th///ZeemeWebApp/memberload?memberid=e31bfb10-19bb-11e7-81f7-e565e26a97f0&fileid=f96a2993-33de-11e7-aef2-f545ba5b73ef"){
            (data,error) in
            self.model.addEyeTemplate(data!)
            self.criteria?.addTemplateModel(key: "M_1", value: self.model)
            self.faceIdentifierView?.criteria = self.criteria
            self.faceIdentifierView?.refresh()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        faceIdentifierView?.startIdentifier()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        faceIdentifierView?.pauseIdentifier()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        faceIdentifierView?.stopCamera()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension FaceRecognizeViewController : FaceIdentifierProcessViewDelegate{
    func onFound(_ identifyFaceArray: NSMutableArray!, image: UIImage!) {
        print(identifyFaceArray)
    }
}

extension FaceRecognizeViewController : EyeBlinkDelegate{
    public func getOpenEyeLabel() -> String! {
        return "Please open your eyes";
    }
    public func getBlinkEyeLabel() -> String! {
        return "Please blink your eyes";
    }
    public func blinkEyeTimeout() -> Int32 {
        return 5;
    }
    
    public func enableBlinkEye() -> Bool {
        return true;
    }
}
