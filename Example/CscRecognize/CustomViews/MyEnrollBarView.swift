//
//  MyEnrollBarView.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 7/3/2560 BE.
//  Copyright © 2560 CocoaPods. All rights reserved.
//

import UIKit
import CscRecognize

@IBDesignable
class MyEnrollBarView: UIView,EnrollBarViewDelegate {
    
    @IBOutlet weak var captureImageView1: UIImageView!
    @IBOutlet weak var captureImageView2: UIImageView!
    @IBOutlet weak var captureImageView3: UIImageView!
    let defaultImage:UIImage = UIImage(named: "default_person")!
    var captureView:[Int:UIView] = [:]
    
    func getTouchView() -> [Int : UIView] {
        captureView = [:]
        captureView[1] = captureImageView1
        captureView[2] = captureImageView2
        captureView[3] = captureImageView3
        return captureView
    }
    
    func resetView() {
        captureImageView1.image = defaultImage
        captureImageView2.image = defaultImage
        captureImageView3.image = defaultImage
    }
    
    func showCaptureImageView(_ image: UIImage!,_ countNo:Int){
        let imageView = captureView[countNo] as! UIImageView
        imageView.alpha = 1.00
        imageView.image = image
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    fileprivate func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        resetView()
    }
    
    // Loads a XIB file into a view and returns this view.
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
}
