//
//  ViewController.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 06/07/2017.
//  Copyright (c) 2017 Pongpop Inkaew. All rights reserved.
//

import UIKit
import CscRecognize

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getPodBundle()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPodBundle(){
        
        let podBundle = Bundle(for: FaceProcessView.self)
        //let podBundle = Bundle.main
        if let bundleURL = podBundle.url(forResource: "CscRecognize", withExtension: "bundle") {
            
            if let bundle = Bundle(url: bundleURL) {
                let vertShaderPathname = bundle.url(forResource: "DirectDisplayShader", withExtension: "vsh")
                print("vertShaderPathname = \(vertShaderPathname)")
            }else {
                
                print("Could not load the bundle")
                
            }
            
        }else {
            
            print("Could not create a path to the bundle")
            
        }
        //let vertShaderPathname = Bundle.main.url(forResource: "DirectDisplayShader", withExtension: "vsh")
        //print("vertShaderPathname = \(vertShaderPathname)")
    }

}

