//
//  HttpDownloader.swift
//  Zeeme
//
//  Created by Pongpop Inkaew on 7/5/2560 BE.
//  Copyright © 2560 Demo. All rights reserved.
//

import Foundation
import Alamofire

enum DownloadFileError : Error{
    
}

class HttpDownloader{
    //static let sharedInstance = HttpDownloader()
    func downloadWith(url:String,_ completion: ((Data?,Error?)->())? = nil ){
        print("Downloading : \(url)")
        Alamofire.request(url)
            .responseData{ response in
                if let data = response.result.value, response.error == nil{
                    print("Downloaded, Size : \(String(describing: data.count)) Bytes, Url : \(url)")
                    completion?(data, nil)
                }
                if let error = response.error{
                    print("Error took place while downloading a file. : \(error.localizedDescription), URL :\(url)");
                    completion?(nil, error)
                }
                
        }
    }
    
}
