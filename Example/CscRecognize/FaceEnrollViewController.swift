//
//  FaceEnrollViewController.swift
//  CscRecognize
//
//  Created by Pongpop Inkaew on 6/11/2560 BE.
//  Copyright © 2560 CocoaPods. All rights reserved.
//

import UIKit
import CscRecognize

class FaceEnrollViewController: UIViewController {
    var mFaceEnrollView:FaceEnrollView?
    @IBOutlet weak var cameraPreview: UIView!
    @IBOutlet weak var enrollImageView: UIImageView!
    @IBOutlet weak var enrollBarView: MyEnrollBarView!
    //@IBOutlet weak var sampleEnrollBarView: MyEnrollBarView!
    let criteria = FaceEnrollCriteria()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mFaceEnrollView = FaceEnrollView()
        mFaceEnrollView?.prepare(cameraPreview)
        mFaceEnrollView?.delegate = self
        mFaceEnrollView?.criteria = criteria
        mFaceEnrollView?.enrollBarViewDelegate = enrollBarView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        mFaceEnrollView?.destroy()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension FaceEnrollViewController : FaceEnrollProcessViewDelegate{
    func onFaceEnrollNotMatch() {
        print("onFaceEnrollNotMatch")
        //mFaceEnrollView?.resetView()
        mFaceEnrollView?.startEnroll()
    }
    func onFaceEnrollResetView() {
        
    }
    func onFaceEnrollCaptureFace(_ image: UIImage!, countNo: Int32) {
        enrollImageView.image = image
        //sampleEnrollBarView.setCaptureImage(image: image)
        print("onFaceEnrollCaptureFace : \(countNo)")
    }
    func onFaceEnrollSuccess(_ faceEnrollViewModel: FaceEnrollViewModel!) {
        print("onFaceEnrollSuccess : \(faceEnrollViewModel?.eyesTemplate.count)")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        mFaceEnrollView?.touchesBegan(touches, with: event)
    }
}


