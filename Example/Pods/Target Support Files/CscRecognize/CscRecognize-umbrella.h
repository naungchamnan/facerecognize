#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CscRecognize.h"
#import "FaceEnrollProcessView.h"
#import "FaceIdentifierProcessView.h"
#import "FaceProcessView.h"
#import "LuxandFaceSDK.h"
#import "RecognitionCamera.h"
#import "RecognitionGLView.h"

FOUNDATION_EXPORT double CscRecognizeVersionNumber;
FOUNDATION_EXPORT const unsigned char CscRecognizeVersionString[];

