# CscRecognize

[![CI Status](http://img.shields.io/travis/Pongpop Inkaew/CscRecognize.svg?style=flat)](https://travis-ci.org/Pongpop Inkaew/CscRecognize)
[![Version](https://img.shields.io/cocoapods/v/CscRecognize.svg?style=flat)](http://cocoapods.org/pods/CscRecognize)
[![License](https://img.shields.io/cocoapods/l/CscRecognize.svg?style=flat)](http://cocoapods.org/pods/CscRecognize)
[![Platform](https://img.shields.io/cocoapods/p/CscRecognize.svg?style=flat)](http://cocoapods.org/pods/CscRecognize)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CscRecognize is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CscRecognize"
```

## Author

Pongpop Inkaew, nice.comsci@gmail.com

## License

CscRecognize is available under the MIT license. See the LICENSE file for more info.
